--------------
bWAPP - README
--------------

bWAPP, or a buggy web application, is a deliberately insecure web application.
bWAPP helps security enthusiasts, developers and students to discover and to prevent web vulnerabilities.
It prepares one to conduct successful penetration testing and ethical hacking projects.
What makes bWAPP so unique? Well, it has over 100 web bugs!
bWAPP covers all major known web vulnerabilities, including all risks from the OWASP Top 10 project!
It is for security-testing and educational purposes only.

It includes:

*/ Injection vulnerabilities like SQL, SSI, XML/XPath, JSON, LDAP, HTML, iFrame, OS Command and SMTP injection
*/ Cross-Site Scripting (XSS), Cross-Site Tracing (XST) and Cross-Site Request Forgery (CSRF)
*/ Unrestricted file uploads and backdoor files
*/ Authentication, authorization and session management issues
*/ Arbitrary file access and directory traversals
*/ Local and remote file inclusions (LFI/RFI)
*/ Server Side Request Forgery (SSRF)
*/ XML External Entity Attacks (XXE)
*/ Heartbleed vulnerability (OpenSSL)
*/ Shellshock vulnerability (CGI)
*/ Drupal SQL injection (Drupageddon)
*/ Configuration issues: Man-in-the-Middle, cross-domain policy file, information disclosures,...
*/ HTTP parameter pollution and HTTP response splitting
*/ Denial-of-Service (DoS) attacks
*/ HTML5 ClickJacking, Cross-Origin Resource Sharing (CORS) and web storage issues
*/ Unvalidated redirects and forwards
*/ Parameter tampering
*/ PHP-CGI vulnerability
*/ Insecure cryptographic storage
*/ AJAX and Web Services issues (JSON/XML/SOAP)
*/ Cookie and password reset poisoning
*/ Insecure FTP, SNMP and WebDAV configurations
*/ and much more...

bWAPP is a PHP application that uses a MySQL database. It can be hosted on Linux and Windows using Apache/IIS and MySQL. It can be installed with WAMP or XAMPP.

It's also possible to download our bee-box, a custom VM pre-installed with bWAPP.

This project is part of the ITSEC GAMES project. ITSEC GAMES are a fun approach to IT security education. 
IT security, ethical hacking, training and fun... all mixed together.
You can find more about the ITSEC GAMES and bWAPP projects on our blog.

We offer a 2-day comprehensive web security course 'Attacking & Defending Web Apps with bWAPP'.
This course can be scheduled on demand, at your location!
More info: http://goo.gl/ASuPa1 (pdf)

Enjoy!

Cheers

Malik Mesellem
Twitter: @MME_IT

Features
* SQL, HTML, iFrame, SSI, OS Command, PHP, XML, XPath, LDAP and SMTP injections
* Blind SQL injection and Blind OS Command injection
* Boolean-based and time-based Blind SQL injections
* Drupageddon and Drupalgeddon2 (CVE-2018-7600)
* AJAX and Web Services issues (JSON/XML/SOAP)
* Heartbleed vulnerability (OpenSSL) + detection script included
* Shellshock vulnerability (CGI)
* Cross-Site Scripting (XSS) and Cross-Site Tracing (XST)
* phpMyAdmin BBCode Tag XSS
* Cross-Site Request Forgery (CSRF)
* Information disclosures: favicons, version info, custom headers,...
* Unrestricted file uploads and backdoor files
* Old, backup & unreferenced files
* Authentication, authorization and session management issues
* Password and CAPTCHA attacks
* Insecure DistCC, FTP, NTP, Samba, SNMP, VNC, WebDAV configurations
* Arbitrary file access with Samba
* Directory traversals and unrestricted file access
* Local and remote file inclusions (LFI/RFI)
* Server Side Request Forgery (SSRF)
* XML External Entity attacks (XXE)
* Man-in-the-Middle attacks (HTTP/SMTP)
* HTTP parameter pollution and HTTP verb tampering
* Denial-of-Service (DoS) attacks: Slow Post, SSL-Exhaustion, XML Bomb,...
* POODLE vulnerability
* BREACH/CRIME/BEAST SSL attacks
* HTML5 ClickJacking and web storage issues
* Insecure iFrame (HTML5 sandboxing)
* Insecure direct object references (parameter tampering)
* Insecure cryptographic storage
* Cross-Origin Resource Sharing (CORS) issues
* Cross-domain policy file attacks (Flash/Silverlight)
* Local privilege escalations: udev, sendpage
* Cookie and password reset poisoning
* Host header attacks: password reset poisoning en cache pollutions
* PHP CGI remote code execution
* Dangerous PHP Eval function
* Local and remote buffer overflows (BOF)
* phpMyAdmin and SQLiteManager vulnerabilities
* Nginx web server vulnerabilities
* HTTP response splitting, unvalidated redirects and forwards
* WSDL SOAP vulnerabilities
* Form-based authentication and No-authentication modes
* Active Directory LDAP integration
* Fuzzing possibilities
* and much more...
* HINT: download our bee-box VM > it has ALL necessary extensions
* bee-box is compatible with VMware and VirtualBox!
Enjoy it little bees ;)
